$(function(){
	var areaModal = $('#area-modal');						//модальное окно
	var areaSelectInModal = $("#area-in-modal");			//селект района на модальном окне
	var areaSelect = $("#area");							//селект района в фильтре
	var areaClassBtn = 	$(".btn-add-area");					//кнопка выбора района при пустом значении фильтра
	var metroSelectInModal = $("#metro-in-modal");			//селект метро на модальном окне
	var metroSelect = $("#metro");							//селект метро в фильтре
	var metroClassBtn =  $(".btn-add-metro");				//кнопка выбора района при пустом значении фильтра
	
	arrTempSelect = new Array ( ); 
	arrmetroTempSelect = new Array ( ); 
		
	window.onload = function () { 							//обнуляем временные массивы и селекты при перезагрузке страницы
		arrTempSelect = new Array ( ); 
		arrmetroTempSelect = new Array ( ); 
		areaSelect.val(arrTempSelect).select2();
		metroSelect.val(arrmetroTempSelect).select2({
				formatSelectionCssClass: colorTags,
			});
	 
	};
	areaSelectInModal.on("change", function(e) { 
			arrTempSelect = areaSelectInModal.val(); 				//при изменении значения в селекте меняем временный массив
			displayBtnAddArea(arrTempSelect, areaClassBtn);  		//отобразить кнопку или селект	 
		});
	metroSelectInModal.on("change", function(e) { 
			arrmetroTempSelect = metroSelectInModal.val(); 			//при изменении значения в селекте меняем временный массив
			displayBtnAddArea(arrmetroTempSelect, metroClassBtn);
		});
	areaSelect.on("change", function(e) { 
			arrTempSelect = areaSelect.val();						//при изменении значения в фильтре также меняем временный массив	
			displayBtnAddArea(arrTempSelect, areaClassBtn);			//отобразить кнопку или селект			 
		}); 
	metroSelect.on("change", function(e) { 
			arrmetroTempSelect = metroSelect.val();					//при изменении значения в фильтре также меняем временный массив	
			displayBtnAddArea(arrmetroTempSelect, metroClassBtn);	//отобразить кнопку или селект
		}); 
 
	
	 
	areaModal.on('shown.bs.modal', function (e) {
			areaSelectInModal.val(arrTempSelect).select2(); 
			metroSelectInModal.val(arrmetroTempSelect).select2({
				formatSelectionCssClass: colorTags,
				
			});  			
			 
		});		
	areaModal.on('hidden.bs.modal', function (e) {
			areaSelect.val(arrTempSelect).select2();
			metroSelect.val(arrmetroTempSelect).select2({
				formatSelectionCssClass: colorTags,
			});
			 
		});	
		
	$('.metrotitle').on('click', function(){
			if(arrmetroTempSelect==null) {
				arrmetroTempSelect = new Array ( );
				arrmetroTempSelect.push(this.id);
			} else {
				arrmetroTempSelect.push(this.id);
			}
			metroSelectInModal.val(arrmetroTempSelect).trigger("change");
		});	
		
		
		
/*---вспомогательные функции НАЧАЛО---*/
	function colorTags (data, container) {			//раскрашивание тегов в зависимости от значения ID
		
			switch (data["id"].charAt(0)) {
			   case 'r':
				  return "red" 
				  break
			   case 'b':
				  return "blue"; 
				  break
			   case 'g':
				  return "green"; 
				  break
			   default:
				  break
			}
		} 
	function displayBtnAddArea(arr, classBtn) {			//отображение кнопки вместо селектов при пустых значениях фильтра
		
		if (arr==null) {
				classBtn.css({'display' : 'block'});
			} else {
				classBtn.css({'display' : 'none'});
				
			} 
	}	
		
/*---вспомогательные функции КОНЕЦ---*/

/*---векторная карта выбора района НАЧАЛО---*/		
 	var r = Raphael('map-area', 500,500),
		attributes = {
            fill: '#60b54c',
            stroke: '#eaf534',
            'stroke-width': 1,
            'stroke-linejoin': 'round',
			'cursor': 'pointer',
			 
        },
		arr = new Array();  
	 
	for (var country in paths) {
		
		var obj = r.path(paths[country].path);
		
		obj.attr(attributes);
		
		arr[obj.id] = country;
		
		$('#'+arr[obj.id]).on('mouseenter', function(){				//анимация области по наведению на  тайтл
			r.getById($.inArray(this.id, arr)).animate({
				fill: '#eaf534'
			}, 300);
		}); 
		$('#'+arr[obj.id]).on('mouseleave', function(){				//анимация области по наведению на  тайтл
			r.getById($.inArray(this.id, arr)).animate({
				fill: attributes.fill
			}, 300);
		
		});
		$('#'+arr[obj.id]).on('click', function(){					//события на тайтл
			if(arrTempSelect==null) {
				arrTempSelect = new Array (areaSelectInModal.val());
				arrTempSelect.push(this.id);
			} else {
				arrTempSelect.push(this.id);
			}
			areaSelectInModal.val(arrTempSelect).trigger("change");
		
		});
		 
		var current = null;
		
		obj
		.hover(function(){
			this.animate({
				fill: '#eaf534'
			}, 300);			
			 $('#'+arr[this.id]).css({'font-Size' : '22px', 'font-weight' : 'bolder', 'color':'#222'}); //анимация тайтла по наведению на область
			
		}, function(){
			this.animate({
				fill: attributes.fill
			}, 300);
			
			  $('#'+arr[this.id]).css({'font-Size' : '12px', 'font-weight' : 'normal', 'color':'#fff'}); //анимация тайтла по наведению на область
		})
		.click(function(){
			//document.location.hash = arr[this.id]; 										//события на область
			
			if(arrTempSelect==null) {
				arrTempSelect = new Array ( );
				arrTempSelect.push(arr[this.id]);
			} else {
				arrTempSelect.push(arr[this.id]);
			}
			
			
			areaSelectInModal.val(arrTempSelect).trigger("change");
			 
			
		});
	}
/*---векторная карта выбора района КОНЕЦ---*/					
});

